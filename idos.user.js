// ==UserScript==
// @name        IDOS
// @namespace   http://majun.com/idos
// @description Add link 'Add to Google calendar' to IDOS
// @version     0.1
// @updateUrl   https://bitbucket.org/majun/idos-user-js/raw/master/idos.user.js
// @include     http://jizdnirady.idnes.cz/detail/*
// @copyright   2014+, Martin Junger
// @grant       none
//
//   jQuery
// @require     http://code.jquery.com/jquery-1.10.1.min.js
//
// @run-at      document-end
// ==/UserScript==

var $jq = jQuery.noConflict();

$jq('#ctl00_anchors').append('<a href="javascript:void(0);" class="ico-add m-textlink" id="addToGoogleCalendar">Přidat do Google kalendáře</a>');
$jq('#addToGoogleCalendar').click(function() {
	var url = 'https://www.google.com/calendar/render?action=TEMPLATE&text=%TITLE%&dates=%DATEFROM%/%DATETO%&details=%DETAILS%&location=%LOC%&pli=1&sf=true&output=xml';

	// 'Praha hl.n. » Česká Třebová'
	var title = $jq('h3').eq(0).text();

	/**
	 * @param string '12.2.2014 Středa'
	 * @param string '21:03'
	 * @return string '20140212T210300'
	 */
	function formatDateTime(date, time){
		var dateArr = date.replace(/ .*/, '').split('.').reverse();
		dateArr[1] = dateArr[1].length == 1 ? '0'+dateArr[1] : dateArr[1];
		dateArr[2] = dateArr[2].length == 1 ? '0'+dateArr[2] : dateArr[2];
		var timeArr = time.split(':');
		timeArr[0] = timeArr[0].length == 1 ? '0'+timeArr[0] : timeArr[0];
		return dateArr.join('') + 'T' + timeArr.join('') + '00';
	};

	var tables = $jq('table.results tbody');
	var transfers = [];

	for(i=0; i < tables.length; i++) {
		var $table = $jq(tables[i]);
		// info: 'Ex 123'
		var $info = $table.find('td[colspan]').eq(0).find('.ico-info-red');
		var info = $info.find('img').eq(0).attr('title').trim() + ' ' + $info.text().replace(/ +/g, ' ').replace(' ,', ',').trim();
		// from: '20140212T210300'
		var datefrom = $jq('table.plain tbody').find('tr td.fareh').next().eq(0).text();
		var timefrom = $table.find('tr.routetrbold td.right').eq(1).text();
		var datefrom = formatDateTime(datefrom, timefrom);
		var placefrom = $table.find('tr.routetrbold td').eq(0).text();

		// to: '20140212T224800'
		var dateto = $jq('table.plain tbody').find('tr td.fareh').next().eq(0).text()
		var timeto = $table.find('tr.routetrbold td.right').eq(4).text().trim();
		if(timeto == '&nbsp;' || timeto == '') {
			timeto = $table.find('tr.routetrbold td.right').eq(5).text();
		}
		var dateto = formatDateTime(dateto, timeto);
		var placeto = $table.find('tr.routetrbold td.right').eq(4).prev().text();

		transfers.push({'info': info.trim(),
			'placefrom': placefrom.trim(), 'datefrom': datefrom, 'timefrom': timefrom,
			'placeto': placeto.trim(), 'dateto': dateto, 'timeto': timeto});
	}

	var datefrom = transfers[0].datefrom;
	var dateto = transfers[ transfers.length - 1].dateto;

	var transfersDetail = [];
	for(i = 0; i < transfers.length; i++) {
		var t = transfers[i];
		transfersDetail.push(t.info + '\n' +
			t.timefrom + ' ' + t.placefrom + '\n' +
			t.timeto + ' ' + t.placeto);
	};

	// 'Doba:	1 hod 45 min;\n '+
	// 'Vzdálenost:	164 km;\n '+
	// 'Cena:	226 Kč / IN50 113 Kč / IN25 170 Kč';
	var details = $jq('table.plain').eq(0).text().replace(/:/g, ': ').replace(/\t/g, '').replace(/\n\n/g, '\n').replace(/^\n/, '').replace(/\n$/, '');
	details += '\n-----\n' + transfersDetail.join('\n');

	var loc = '';

	url = url.replace('%TITLE%', encodeURIComponent(title));
	url = url.replace('%DATEFROM%', encodeURIComponent(datefrom));
	url = url.replace('%DATETO%', encodeURIComponent(dateto));
	url = url.replace('%DETAILS%', encodeURIComponent(details));
	url = url.replace('%LOC%', encodeURIComponent(loc));

	window.open(url, '_blank');
});
